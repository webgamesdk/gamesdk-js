import { EventEmitter } from "./event/EventEmitter";
import { GameClient } from "./GameClient"

export class Room extends EventEmitter implements RoomMeta{

    constructor(
        public id,
        public configuration,
        public client
    ){
        super()
    }


    send(event: string, data: any){ 
        this.client.send(event, data, "ROOM:" + this.id)
    }

}

export const RoomEvent = {
    DESTROY: "DESTROY",
}