const path = require("path")

module.exports = {

    mode: "production",
    entry: "./src/index.ts",

    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "gamesdk.js",
        library: {
            root: "",
            umd: "gamesdk-js"
        },
        libraryTarget: "umd"
    },

    resolve: {
        extensions: [".ts"]
    },

    module: {
        rules: [
            { 
                test: /\.ts(x?)$/, 
                loader: "ts-loader", 
                exclude: /node_modules/
            },
        ]
    }

}