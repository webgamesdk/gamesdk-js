"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const EventEmitter_1 = require("./event/EventEmitter");
const Room_1 = require("./Room");
const isomorphic_ws_1 = __importDefault(require("isomorphic-ws"));
class GameClient extends EventEmitter_1.EventEmitter {
    constructor(url) {
        super();
        this.url = url;
        this.session_id = "";
        this.room = null;
        this.socket = new isomorphic_ws_1.default(url);
        this.setupSocket();
    }
    createRoom(name) {
        const configuration = {
            name: name
        };
        return new Promise(resolve => {
            this.once("room created", id => {
                console.log("id: ", id);
                this.room = new Room_1.Room(id, configuration, this);
                resolve(this.room);
            });
            this.send("room create", configuration);
        });
    }
    joinRoom(id) {
        return new Promise(resolve => {
            this.once("room joined", roomData => {
                console.log("data", roomData);
                this.room = new Room_1.Room(roomData.id, roomData.configuration, this);
                resolve(this.room);
            });
            this.send("room join", id);
        });
    }
    setupSocket() {
        this.socket.onopen = ev => {
            this.send("register");
        };
        this.socket.onmessage = ev => {
            const message = JSON.parse(ev.data);
            this.session_id = message.session_id;
            let data;
            try {
                data = JSON.parse(message.data);
            }
            catch (e) {
                data = message.data;
            }
            if (message.context === "ALL") {
                this.call(message.event, data);
            }
            else if (message.context === "ROOM") {
                if (this.room) {
                    this.room.call(message.event, data);
                }
            }
        };
        this.socket.onerror = err => {
            console.error(err);
        };
        this.socket.onclose = () => {
            console.log("Conenction closed");
        };
    }
    send(event, data = null, context = "ALL") {
        let msg = data;
        if (data instanceof Object)
            msg = JSON.stringify(data);
        // TODO: Check if socket is connected
        this.socket.send(JSON.stringify({
            event: event,
            session_id: this.session_id,
            context: context,
            data: msg
        }));
    }
}
exports.GameClient = GameClient;
//# sourceMappingURL=GameClient.js.map