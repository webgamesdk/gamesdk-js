import { EventEmitter } from "./event/EventEmitter";
import { Room } from "./Room";
import WebSocket = require("isomorphic-ws")
 
export class GameClient extends EventEmitter{

    session_id: String = ""
    room: Room = null
    socket: WebSocket

    constructor(public url: string, public onConnected: (reconnected: boolean) => void){
        super()
        try{
            if(new Date(sessionStorage.getItem("sessionIdExpire")).getTime() < new Date().getTime()){
                sessionStorage.removeItem("sessionId")
                sessionStorage.removeItem("sessionIdExpire")
            }
            this.session_id = sessionStorage.getItem("sessionId") ? sessionStorage.getItem("sessionId") : ""
        }catch(ev){}
        this.socket = new WebSocket(url)
        this.setupSocket();
    } 
 
    
    createRoom(configuration: any): Promise<Room>{
        return new Promise<Room>((resolve, reject) => {
            this.once("room created", id => {
                console.debug("id: ", id)
                this.room = new Room(id, configuration, this)
                resolve(this.room)
            })
            this.once("room create error", reject)
            this.send("room create", configuration)
        })
    }

    joinRoom(id): Promise<{ room: Room, reconnected: boolean }>{
        return new Promise<{ room: Room, reconnected: boolean }>(resolve => {
            const createRoom = (roomData, reconnected) => {
                console.debug("data", roomData)
                this.room = new Room(roomData.id, roomData.configuration, this)
                resolve({room: this.room, reconnected: reconnected})
            }
            this.once("room joined", data => { createRoom(data, false) })
            this.once("room reconnect", data => { createRoom(data, true) })
            this.once("room join error", error => { console.error("Error while joining room " + id, error) })

            this.send("room join", {
                id: id,
                data: {}
            })
        })
    }

    getRooms(): Promise<RoomMeta[]>{
        return new Promise<RoomMeta[]>(resolve => {
            this.once("room list", rooms => {
                console.log("Roooooms", rooms)
            })
            this.send("room list")
        })
    }
    
    setupSocket(){
        this.socket.onopen = ev => {
            this.send("register")
        }
        this.socket.onmessage = ev => {
            const message = JSON.parse(ev.data.toString())
            this.session_id = message.session_id
            let data: any
            try{
                data = JSON.parse(message.data)
            }catch(e){
                data = message.data
            }
            if(message.context === "ALL"){
                this.call(message.event, data)
            }else if(message.context === "ROOM"){
                if(this.room){
                this.room.call(message.event, data)
                }
            }
        }
        this.socket.onerror = err => {
            console.error(err)
        }
        this.socket.onclose = (ev) => {
            console.debug("Conenction closed", ev)
        }

        this.once("reconnect", (data) => {
            this.onConnected(true)
        })
        this.once("registered", sessionId => {
            // keep session id for 3 days
            sessionStorage.setItem("sessionId", sessionId)
            sessionStorage.setItem("sessionIdExpire", new Date(new Date().getTime() + 1000*60*60*24*3).toUTCString())
            this.session_id = sessionId
            this.onConnected(false)
        })
        this.on("error", error => {
            console.error(error)
        })
    }

    
    send(event: string, data: any = null, context: string = "ALL"){
        let msg = data
        if(data instanceof Object)
            msg = JSON.stringify(data)
        console.debug("sending event", event, data)

            // TODO: Check if socket is connected
            this.socket.send(JSON.stringify({
            event: event,
            session_id: this.session_id,
            context: context,
            data: msg
        }))
    }
    
}