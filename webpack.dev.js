const path = require("path")

module.exports = {

    mode: "development",
    entry: "./src/index.ts",
    devtool: "inline-source-map",

    output: {
        path: path.resolve(__dirname, "dist"),
        filename: "gamesdk.js",
        library: "gamesdk",
        libraryTarget: "umd",
        globalObject: "this"
    },

    resolve: {
        extensions: [".ts"],
    },

    module: {
        rules: [
            { 
                test: /\.ts(x?)$/, 
                loader: "ts-loader", 
                exclude: /node_modules/
            },
        ]
    },
    externals: {
        "ws": {
            commonjs: "ws",
            commonjs2: "ws",
            amd: "ws",
            root: "WebSocket"
        }
    }

}