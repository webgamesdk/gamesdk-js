"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const EventEmitter_1 = require("./event/EventEmitter");
class Room extends EventEmitter_1.EventEmitter {
    constructor(id, configuration, client) {
        super();
        this.id = id;
        this.configuration = configuration;
        this.client = client;
    }
    send(event, data) {
        this.client.send(event, data, "ROOM:" + this.id);
    }
}
exports.Room = Room;
//# sourceMappingURL=Room.js.map