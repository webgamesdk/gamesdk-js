"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class EventEmitter {
    constructor() {
        this.eventHandlers = {};
    }
    on(event, func) {
        if (!event || !func)
            return;
        if (!this.eventHandlers[event]) {
            this.eventHandlers[event] = [];
        }
        this.eventHandlers[event].push(func);
    }
    once(event, callback) {
        this.on("#once_" + event, callback);
    }
    call(event, data) {
        if (this.eventHandlers[event]) {
            this.eventHandlers[event].forEach(func => func(data));
        }
        if (this.eventHandlers["*"]) {
            this.eventHandlers["*"].forEach(func => func(data, event));
        }
        if (this.eventHandlers["#once_" + event]) {
            const funcs = this.eventHandlers["#once_" + event];
            for (let i = 0; i < funcs.length; i++) {
                funcs[i](data);
                this.eventHandlers["#once_" + event].splice(i, 1);
            }
        }
    }
    removeEventHandler(event, func) {
        if (this.eventHandlers[event]) {
            const index = this.eventHandlers[event].indexOf(f => f === func);
            if (index >= 0) {
                this.eventHandlers[event].splice(index, 1);
            }
        }
    }
    removeEventHandlers(event) {
        this.eventHandlers[event] = [];
    }
}
exports.EventEmitter = EventEmitter;
//# sourceMappingURL=EventEmitter.js.map