interface RoomMeta{
    id: string,
    configuration: any,
    playersCount?: number
}