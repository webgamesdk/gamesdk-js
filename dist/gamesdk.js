(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["gamesdk"] = factory();
	else
		root["gamesdk"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/isomorphic-ws/browser.js":
/*!***********************************************!*\
  !*** ./node_modules/isomorphic-ws/browser.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {// https://github.com/maxogden/websocket-stream/blob/48dc3ddf943e5ada668c31ccd94e9186f02fafbd/ws-fallback.js

var ws = null

if (typeof WebSocket !== 'undefined') {
  ws = WebSocket
} else if (typeof MozWebSocket !== 'undefined') {
  ws = MozWebSocket
} else if (typeof global !== 'undefined') {
  ws = global.WebSocket || global.MozWebSocket
} else if (typeof window !== 'undefined') {
  ws = window.WebSocket || window.MozWebSocket
} else if (typeof self !== 'undefined') {
  ws = self.WebSocket || self.MozWebSocket
}

module.exports = ws

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ "./src/GameClient.ts":
/*!***************************!*\
  !*** ./src/GameClient.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const EventEmitter_1 = __webpack_require__(/*! ./event/EventEmitter */ "./src/event/EventEmitter.ts");
const Room_1 = __webpack_require__(/*! ./Room */ "./src/Room.ts");
const WebSocket = __webpack_require__(/*! isomorphic-ws */ "./node_modules/isomorphic-ws/browser.js");
class GameClient extends EventEmitter_1.EventEmitter {
    constructor(url, onConnected) {
        super();
        this.url = url;
        this.onConnected = onConnected;
        this.session_id = "";
        this.room = null;
        try {
            if (new Date(sessionStorage.getItem("sessionIdExpire")).getTime() < new Date().getTime()) {
                sessionStorage.removeItem("sessionId");
                sessionStorage.removeItem("sessionIdExpire");
            }
            this.session_id = sessionStorage.getItem("sessionId") ? sessionStorage.getItem("sessionId") : "";
        }
        catch (ev) { }
        this.socket = new WebSocket(url);
        this.setupSocket();
    }
    createRoom(configuration) {
        return new Promise((resolve, reject) => {
            this.once("room created", id => {
                console.debug("id: ", id);
                this.room = new Room_1.Room(id, configuration, this);
                resolve(this.room);
            });
            this.once("room create error", reject);
            this.send("room create", configuration);
        });
    }
    joinRoom(id) {
        return new Promise(resolve => {
            const createRoom = (roomData, reconnected) => {
                console.debug("data", roomData);
                this.room = new Room_1.Room(roomData.id, roomData.configuration, this);
                resolve({ room: this.room, reconnected: reconnected });
            };
            this.once("room joined", data => { createRoom(data, false); });
            this.once("room reconnect", data => { createRoom(data, true); });
            this.once("room join error", error => { console.error("Error while joining room " + id, error); });
            this.send("room join", {
                id: id,
                data: {}
            });
        });
    }
    getRooms() {
        return new Promise(resolve => {
            this.once("room list", rooms => {
                console.log("Roooooms", rooms);
            });
            this.send("room list");
        });
    }
    setupSocket() {
        this.socket.onopen = ev => {
            this.send("register");
        };
        this.socket.onmessage = ev => {
            const message = JSON.parse(ev.data.toString());
            this.session_id = message.session_id;
            let data;
            try {
                data = JSON.parse(message.data);
            }
            catch (e) {
                data = message.data;
            }
            if (message.context === "ALL") {
                this.call(message.event, data);
            }
            else if (message.context === "ROOM") {
                if (this.room) {
                    this.room.call(message.event, data);
                }
            }
        };
        this.socket.onerror = err => {
            console.error(err);
        };
        this.socket.onclose = (ev) => {
            console.debug("Conenction closed", ev);
        };
        this.once("reconnect", (data) => {
            this.onConnected(true);
        });
        this.once("registered", sessionId => {
            // keep session id for 3 days
            sessionStorage.setItem("sessionId", sessionId);
            sessionStorage.setItem("sessionIdExpire", new Date(new Date().getTime() + 1000 * 60 * 60 * 24 * 3).toUTCString());
            this.session_id = sessionId;
            this.onConnected(false);
        });
        this.on("error", error => {
            console.error(error);
        });
    }
    send(event, data = null, context = "ALL") {
        let msg = data;
        if (data instanceof Object)
            msg = JSON.stringify(data);
        console.debug("sending event", event, data);
        // TODO: Check if socket is connected
        this.socket.send(JSON.stringify({
            event: event,
            session_id: this.session_id,
            context: context,
            data: msg
        }));
    }
}
exports.GameClient = GameClient;


/***/ }),

/***/ "./src/Room.ts":
/*!*********************!*\
  !*** ./src/Room.ts ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
const EventEmitter_1 = __webpack_require__(/*! ./event/EventEmitter */ "./src/event/EventEmitter.ts");
class Room extends EventEmitter_1.EventEmitter {
    constructor(id, configuration, client) {
        super();
        this.id = id;
        this.configuration = configuration;
        this.client = client;
    }
    send(event, data) {
        this.client.send(event, data, "ROOM:" + this.id);
    }
}
exports.Room = Room;
exports.RoomEvent = {
    DESTROY: "DESTROY",
};


/***/ }),

/***/ "./src/event/EventEmitter.ts":
/*!***********************************!*\
  !*** ./src/event/EventEmitter.ts ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
class EventEmitter {
    constructor() {
        this.eventHandlers = {};
    }
    on(event, func) {
        if (!event || !func)
            return;
        if (!this.eventHandlers[event]) {
            this.eventHandlers[event] = [];
        }
        this.eventHandlers[event].push(func);
    }
    once(event, callback) {
        this.on("#once_" + event, callback);
    }
    call(event, data) {
        if (this.eventHandlers[event]) {
            this.eventHandlers[event].forEach(func => func(data));
        }
        if (this.eventHandlers["*"]) {
            this.eventHandlers["*"].forEach(func => func(data, event));
        }
        if (this.eventHandlers["#once_" + event]) {
            const funcs = this.eventHandlers["#once_" + event];
            for (let i = 0; i < funcs.length; i++) {
                funcs[i](data);
                this.eventHandlers["#once_" + event].splice(i, 1);
            }
        }
    }
    removeEventHandler(event, func) {
        if (this.eventHandlers[event]) {
            const index = this.eventHandlers[event].indexOf(f => f === func);
            if (index >= 0) {
                this.eventHandlers[event].splice(index, 1);
            }
        }
    }
    removeEventHandlers(event) {
        this.eventHandlers[event] = [];
    }
}
exports.EventEmitter = EventEmitter;


/***/ }),

/***/ "./src/index.ts":
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var GameClient_1 = __webpack_require__(/*! ./GameClient */ "./src/GameClient.ts");
exports.GameClient = GameClient_1.GameClient;
var Room_1 = __webpack_require__(/*! ./Room */ "./src/Room.ts");
exports.Room = Room_1.Room;
exports.RoomEvent = Room_1.RoomEvent;
var EventEmitter_1 = __webpack_require__(/*! ./event/EventEmitter */ "./src/event/EventEmitter.ts");
exports.EventEmitter = EventEmitter_1.EventEmitter;


/***/ })

/******/ });
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9nYW1lc2RrL3dlYnBhY2svdW5pdmVyc2FsTW9kdWxlRGVmaW5pdGlvbiIsIndlYnBhY2s6Ly9nYW1lc2RrL3dlYnBhY2svYm9vdHN0cmFwIiwid2VicGFjazovL2dhbWVzZGsvLi9ub2RlX21vZHVsZXMvaXNvbW9ycGhpYy13cy9icm93c2VyLmpzIiwid2VicGFjazovL2dhbWVzZGsvKHdlYnBhY2spL2J1aWxkaW4vZ2xvYmFsLmpzIiwid2VicGFjazovL2dhbWVzZGsvLi9zcmMvR2FtZUNsaWVudC50cyIsIndlYnBhY2s6Ly9nYW1lc2RrLy4vc3JjL1Jvb20udHMiLCJ3ZWJwYWNrOi8vZ2FtZXNkay8uL3NyYy9ldmVudC9FdmVudEVtaXR0ZXIudHMiLCJ3ZWJwYWNrOi8vZ2FtZXNkay8uL3NyYy9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0QsTztRQ1ZBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7O0FDbEZBOztBQUVBOztBQUVBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQSxDQUFDO0FBQ0Q7QUFDQSxDQUFDO0FBQ0Q7QUFDQSxDQUFDO0FBQ0Q7QUFDQTs7QUFFQTs7Ozs7Ozs7Ozs7OztBQ2hCQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBLENBQUM7QUFDRDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLDRDQUE0Qzs7QUFFNUM7Ozs7Ozs7Ozs7Ozs7OztBQ25CQSxzR0FBb0Q7QUFDcEQsa0VBQThCO0FBQzlCLHNHQUEyQztBQUUzQyxNQUFhLFVBQVcsU0FBUSwyQkFBWTtJQU14QyxZQUFtQixHQUFXLEVBQVMsV0FBMkM7UUFDOUUsS0FBSyxFQUFFO1FBRFEsUUFBRyxHQUFILEdBQUcsQ0FBUTtRQUFTLGdCQUFXLEdBQVgsV0FBVyxDQUFnQztRQUpsRixlQUFVLEdBQVcsRUFBRTtRQUN2QixTQUFJLEdBQVMsSUFBSTtRQUtiLElBQUc7WUFDQyxJQUFHLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLE9BQU8sRUFBRSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLEVBQUM7Z0JBQ3BGLGNBQWMsQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDO2dCQUN0QyxjQUFjLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDO2FBQy9DO1lBQ0QsSUFBSSxDQUFDLFVBQVUsR0FBRyxjQUFjLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFO1NBQ25HO1FBQUEsT0FBTSxFQUFFLEVBQUMsR0FBRTtRQUNaLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxTQUFTLENBQUMsR0FBRyxDQUFDO1FBQ2hDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBR0QsVUFBVSxDQUFDLGFBQWtCO1FBQ3pCLE9BQU8sSUFBSSxPQUFPLENBQU8sQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDekMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsRUFBRSxDQUFDLEVBQUU7Z0JBQzNCLE9BQU8sQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQztnQkFDekIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLFdBQUksQ0FBQyxFQUFFLEVBQUUsYUFBYSxFQUFFLElBQUksQ0FBQztnQkFDN0MsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDdEIsQ0FBQyxDQUFDO1lBQ0YsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxNQUFNLENBQUM7WUFDdEMsSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsYUFBYSxDQUFDO1FBQzNDLENBQUMsQ0FBQztJQUNOLENBQUM7SUFFRCxRQUFRLENBQUMsRUFBRTtRQUNQLE9BQU8sSUFBSSxPQUFPLENBQXVDLE9BQU8sQ0FBQyxFQUFFO1lBQy9ELE1BQU0sVUFBVSxHQUFHLENBQUMsUUFBUSxFQUFFLFdBQVcsRUFBRSxFQUFFO2dCQUN6QyxPQUFPLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUM7Z0JBQy9CLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxXQUFJLENBQUMsUUFBUSxDQUFDLEVBQUUsRUFBRSxRQUFRLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQztnQkFDL0QsT0FBTyxDQUFDLEVBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBQyxDQUFDO1lBQ3hELENBQUM7WUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsRUFBRSxHQUFHLFVBQVUsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLEVBQUMsQ0FBQyxDQUFDO1lBQzdELElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLEVBQUUsR0FBRyxVQUFVLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxFQUFDLENBQUMsQ0FBQztZQUMvRCxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLEtBQUssQ0FBQyxFQUFFLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQywyQkFBMkIsR0FBRyxFQUFFLEVBQUUsS0FBSyxDQUFDLEVBQUMsQ0FBQyxDQUFDO1lBRWpHLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNuQixFQUFFLEVBQUUsRUFBRTtnQkFDTixJQUFJLEVBQUUsRUFBRTthQUNYLENBQUM7UUFDTixDQUFDLENBQUM7SUFDTixDQUFDO0lBRUQsUUFBUTtRQUNKLE9BQU8sSUFBSSxPQUFPLENBQWEsT0FBTyxDQUFDLEVBQUU7WUFDckMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLEVBQUU7Z0JBQzNCLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQztZQUNsQyxDQUFDLENBQUM7WUFDRixJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUMxQixDQUFDLENBQUM7SUFDTixDQUFDO0lBRUQsV0FBVztRQUNQLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQyxFQUFFO1lBQ3RCLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQ3pCLENBQUM7UUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUMsRUFBRTtZQUN6QixNQUFNLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDOUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUMsVUFBVTtZQUNwQyxJQUFJLElBQVM7WUFDYixJQUFHO2dCQUNDLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUM7YUFDbEM7WUFBQSxPQUFNLENBQUMsRUFBQztnQkFDTCxJQUFJLEdBQUcsT0FBTyxDQUFDLElBQUk7YUFDdEI7WUFDRCxJQUFHLE9BQU8sQ0FBQyxPQUFPLEtBQUssS0FBSyxFQUFDO2dCQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDO2FBQ2pDO2lCQUFLLElBQUcsT0FBTyxDQUFDLE9BQU8sS0FBSyxNQUFNLEVBQUM7Z0JBQ2hDLElBQUcsSUFBSSxDQUFDLElBQUksRUFBQztvQkFDYixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQztpQkFDbEM7YUFDSjtRQUNMLENBQUM7UUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsRUFBRTtZQUN4QixPQUFPLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztRQUN0QixDQUFDO1FBQ0QsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsQ0FBQyxFQUFFLEVBQUUsRUFBRTtZQUN6QixPQUFPLENBQUMsS0FBSyxDQUFDLG1CQUFtQixFQUFFLEVBQUUsQ0FBQztRQUMxQyxDQUFDO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLEVBQUUsRUFBRTtZQUM1QixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQztRQUMxQixDQUFDLENBQUM7UUFDRixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxTQUFTLENBQUMsRUFBRTtZQUNoQyw2QkFBNkI7WUFDN0IsY0FBYyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsU0FBUyxDQUFDO1lBQzlDLGNBQWMsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsR0FBRyxJQUFJLEdBQUMsRUFBRSxHQUFDLEVBQUUsR0FBQyxFQUFFLEdBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDekcsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTO1lBQzNCLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO1FBQzNCLENBQUMsQ0FBQztRQUNGLElBQUksQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLEtBQUssQ0FBQyxFQUFFO1lBQ3JCLE9BQU8sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQ3hCLENBQUMsQ0FBQztJQUNOLENBQUM7SUFHRCxJQUFJLENBQUMsS0FBYSxFQUFFLE9BQVksSUFBSSxFQUFFLFVBQWtCLEtBQUs7UUFDekQsSUFBSSxHQUFHLEdBQUcsSUFBSTtRQUNkLElBQUcsSUFBSSxZQUFZLE1BQU07WUFDckIsR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO1FBQzlCLE9BQU8sQ0FBQyxLQUFLLENBQUMsZUFBZSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUM7UUFFdkMscUNBQXFDO1FBQ3JDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDaEMsS0FBSyxFQUFFLEtBQUs7WUFDWixVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVU7WUFDM0IsT0FBTyxFQUFFLE9BQU87WUFDaEIsSUFBSSxFQUFFLEdBQUc7U0FDWixDQUFDLENBQUM7SUFDUCxDQUFDO0NBRUo7QUF0SEQsZ0NBc0hDOzs7Ozs7Ozs7Ozs7Ozs7QUMxSEQsc0dBQW9EO0FBR3BELE1BQWEsSUFBSyxTQUFRLDJCQUFZO0lBRWxDLFlBQ1csRUFBRSxFQUNGLGFBQWEsRUFDYixNQUFNO1FBRWIsS0FBSyxFQUFFO1FBSkEsT0FBRSxHQUFGLEVBQUU7UUFDRixrQkFBYSxHQUFiLGFBQWE7UUFDYixXQUFNLEdBQU4sTUFBTTtJQUdqQixDQUFDO0lBR0QsSUFBSSxDQUFDLEtBQWEsRUFBRSxJQUFTO1FBQ3pCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsT0FBTyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUM7SUFDcEQsQ0FBQztDQUVKO0FBZkQsb0JBZUM7QUFFWSxpQkFBUyxHQUFHO0lBQ3JCLE9BQU8sRUFBRSxTQUFTO0NBQ3JCOzs7Ozs7Ozs7Ozs7Ozs7QUN0QkQsTUFBYSxZQUFZO0lBQXpCO1FBRUUsa0JBQWEsR0FBRyxFQUFFO0lBMkNwQixDQUFDO0lBekNDLEVBQUUsQ0FBQyxLQUFhLEVBQUUsSUFBd0M7UUFDeEQsSUFBRyxDQUFDLEtBQUssSUFBSSxDQUFDLElBQUk7WUFBRSxPQUFNO1FBQzFCLElBQUcsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxFQUFDO1lBQzVCLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRTtTQUMvQjtRQUNELElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztJQUN0QyxDQUFDO0lBRUQsSUFBSSxDQUFDLEtBQWEsRUFBRSxRQUE2QjtRQUMvQyxJQUFJLENBQUMsRUFBRSxDQUFDLFFBQVEsR0FBRyxLQUFLLEVBQUUsUUFBUSxDQUFDO0lBQ3JDLENBQUM7SUFFRCxJQUFJLENBQUMsS0FBYSxFQUFFLElBQVM7UUFDM0IsSUFBRyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxFQUFDO1lBQzNCLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3REO1FBQ0QsSUFBRyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxFQUFDO1lBQ3pCLElBQUksQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztTQUMzRDtRQUNELElBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLEVBQUM7WUFDdEMsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1lBQ2xELEtBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFDO2dCQUNuQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUNkLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQ2xEO1NBQ0Y7SUFDSCxDQUFDO0lBRUQsa0JBQWtCLENBQUMsS0FBYSxFQUFFLElBQUk7UUFDcEMsSUFBRyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxFQUFDO1lBQzNCLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQztZQUNoRSxJQUFHLEtBQUssSUFBSSxDQUFDLEVBQUM7Z0JBQ1osSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQzthQUMzQztTQUNGO0lBQ0gsQ0FBQztJQUVELG1CQUFtQixDQUFDLEtBQWE7UUFDL0IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFO0lBQ2hDLENBQUM7Q0FFRjtBQTdDRCxvQ0E2Q0M7Ozs7Ozs7Ozs7Ozs7OztBQzdDRCxrRkFBeUM7QUFBaEMsNENBQVU7QUFDbkIsZ0VBQXdDO0FBQS9CLDBCQUFJO0FBQUUsb0NBQVM7QUFDeEIsb0dBQW1EO0FBQTFDLGtEQUFZIiwiZmlsZSI6ImdhbWVzZGsuanMiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gd2VicGFja1VuaXZlcnNhbE1vZHVsZURlZmluaXRpb24ocm9vdCwgZmFjdG9yeSkge1xuXHRpZih0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcgJiYgdHlwZW9mIG1vZHVsZSA9PT0gJ29iamVjdCcpXG5cdFx0bW9kdWxlLmV4cG9ydHMgPSBmYWN0b3J5KCk7XG5cdGVsc2UgaWYodHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kKVxuXHRcdGRlZmluZShbXSwgZmFjdG9yeSk7XG5cdGVsc2UgaWYodHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnKVxuXHRcdGV4cG9ydHNbXCJnYW1lc2RrXCJdID0gZmFjdG9yeSgpO1xuXHRlbHNlXG5cdFx0cm9vdFtcImdhbWVzZGtcIl0gPSBmYWN0b3J5KCk7XG59KSh0aGlzLCBmdW5jdGlvbigpIHtcbnJldHVybiAiLCIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9pbmRleC50c1wiKTtcbiIsIi8vIGh0dHBzOi8vZ2l0aHViLmNvbS9tYXhvZ2Rlbi93ZWJzb2NrZXQtc3RyZWFtL2Jsb2IvNDhkYzNkZGY5NDNlNWFkYTY2OGMzMWNjZDk0ZTkxODZmMDJmYWZiZC93cy1mYWxsYmFjay5qc1xuXG52YXIgd3MgPSBudWxsXG5cbmlmICh0eXBlb2YgV2ViU29ja2V0ICE9PSAndW5kZWZpbmVkJykge1xuICB3cyA9IFdlYlNvY2tldFxufSBlbHNlIGlmICh0eXBlb2YgTW96V2ViU29ja2V0ICE9PSAndW5kZWZpbmVkJykge1xuICB3cyA9IE1veldlYlNvY2tldFxufSBlbHNlIGlmICh0eXBlb2YgZ2xvYmFsICE9PSAndW5kZWZpbmVkJykge1xuICB3cyA9IGdsb2JhbC5XZWJTb2NrZXQgfHwgZ2xvYmFsLk1veldlYlNvY2tldFxufSBlbHNlIGlmICh0eXBlb2Ygd2luZG93ICE9PSAndW5kZWZpbmVkJykge1xuICB3cyA9IHdpbmRvdy5XZWJTb2NrZXQgfHwgd2luZG93Lk1veldlYlNvY2tldFxufSBlbHNlIGlmICh0eXBlb2Ygc2VsZiAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgd3MgPSBzZWxmLldlYlNvY2tldCB8fCBzZWxmLk1veldlYlNvY2tldFxufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHdzXG4iLCJ2YXIgZztcblxuLy8gVGhpcyB3b3JrcyBpbiBub24tc3RyaWN0IG1vZGVcbmcgPSAoZnVuY3Rpb24oKSB7XG5cdHJldHVybiB0aGlzO1xufSkoKTtcblxudHJ5IHtcblx0Ly8gVGhpcyB3b3JrcyBpZiBldmFsIGlzIGFsbG93ZWQgKHNlZSBDU1ApXG5cdGcgPSBnIHx8IG5ldyBGdW5jdGlvbihcInJldHVybiB0aGlzXCIpKCk7XG59IGNhdGNoIChlKSB7XG5cdC8vIFRoaXMgd29ya3MgaWYgdGhlIHdpbmRvdyByZWZlcmVuY2UgaXMgYXZhaWxhYmxlXG5cdGlmICh0eXBlb2Ygd2luZG93ID09PSBcIm9iamVjdFwiKSBnID0gd2luZG93O1xufVxuXG4vLyBnIGNhbiBzdGlsbCBiZSB1bmRlZmluZWQsIGJ1dCBub3RoaW5nIHRvIGRvIGFib3V0IGl0Li4uXG4vLyBXZSByZXR1cm4gdW5kZWZpbmVkLCBpbnN0ZWFkIG9mIG5vdGhpbmcgaGVyZSwgc28gaXQnc1xuLy8gZWFzaWVyIHRvIGhhbmRsZSB0aGlzIGNhc2UuIGlmKCFnbG9iYWwpIHsgLi4ufVxuXG5tb2R1bGUuZXhwb3J0cyA9IGc7XG4iLCJpbXBvcnQgeyBFdmVudEVtaXR0ZXIgfSBmcm9tIFwiLi9ldmVudC9FdmVudEVtaXR0ZXJcIjtcclxuaW1wb3J0IHsgUm9vbSB9IGZyb20gXCIuL1Jvb21cIjtcclxuaW1wb3J0IFdlYlNvY2tldCA9IHJlcXVpcmUoXCJpc29tb3JwaGljLXdzXCIpXHJcbiBcclxuZXhwb3J0IGNsYXNzIEdhbWVDbGllbnQgZXh0ZW5kcyBFdmVudEVtaXR0ZXJ7XHJcblxyXG4gICAgc2Vzc2lvbl9pZDogU3RyaW5nID0gXCJcIlxyXG4gICAgcm9vbTogUm9vbSA9IG51bGxcclxuICAgIHNvY2tldDogV2ViU29ja2V0XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIHVybDogc3RyaW5nLCBwdWJsaWMgb25Db25uZWN0ZWQ6IChyZWNvbm5lY3RlZDogYm9vbGVhbikgPT4gdm9pZCl7XHJcbiAgICAgICAgc3VwZXIoKVxyXG4gICAgICAgIHRyeXtcclxuICAgICAgICAgICAgaWYobmV3IERhdGUoc2Vzc2lvblN0b3JhZ2UuZ2V0SXRlbShcInNlc3Npb25JZEV4cGlyZVwiKSkuZ2V0VGltZSgpIDwgbmV3IERhdGUoKS5nZXRUaW1lKCkpe1xyXG4gICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2UucmVtb3ZlSXRlbShcInNlc3Npb25JZFwiKVxyXG4gICAgICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2UucmVtb3ZlSXRlbShcInNlc3Npb25JZEV4cGlyZVwiKVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuc2Vzc2lvbl9pZCA9IHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oXCJzZXNzaW9uSWRcIikgPyBzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKFwic2Vzc2lvbklkXCIpIDogXCJcIlxyXG4gICAgICAgIH1jYXRjaChldil7fVxyXG4gICAgICAgIHRoaXMuc29ja2V0ID0gbmV3IFdlYlNvY2tldCh1cmwpXHJcbiAgICAgICAgdGhpcy5zZXR1cFNvY2tldCgpO1xyXG4gICAgfSBcclxuIFxyXG4gICAgXHJcbiAgICBjcmVhdGVSb29tKGNvbmZpZ3VyYXRpb246IGFueSk6IFByb21pc2U8Um9vbT57XHJcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlPFJvb20+KChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5vbmNlKFwicm9vbSBjcmVhdGVkXCIsIGlkID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUuZGVidWcoXCJpZDogXCIsIGlkKVxyXG4gICAgICAgICAgICAgICAgdGhpcy5yb29tID0gbmV3IFJvb20oaWQsIGNvbmZpZ3VyYXRpb24sIHRoaXMpXHJcbiAgICAgICAgICAgICAgICByZXNvbHZlKHRoaXMucm9vbSlcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgdGhpcy5vbmNlKFwicm9vbSBjcmVhdGUgZXJyb3JcIiwgcmVqZWN0KVxyXG4gICAgICAgICAgICB0aGlzLnNlbmQoXCJyb29tIGNyZWF0ZVwiLCBjb25maWd1cmF0aW9uKVxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgam9pblJvb20oaWQpOiBQcm9taXNlPHsgcm9vbTogUm9vbSwgcmVjb25uZWN0ZWQ6IGJvb2xlYW4gfT57XHJcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlPHsgcm9vbTogUm9vbSwgcmVjb25uZWN0ZWQ6IGJvb2xlYW4gfT4ocmVzb2x2ZSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGNyZWF0ZVJvb20gPSAocm9vbURhdGEsIHJlY29ubmVjdGVkKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zb2xlLmRlYnVnKFwiZGF0YVwiLCByb29tRGF0YSlcclxuICAgICAgICAgICAgICAgIHRoaXMucm9vbSA9IG5ldyBSb29tKHJvb21EYXRhLmlkLCByb29tRGF0YS5jb25maWd1cmF0aW9uLCB0aGlzKVxyXG4gICAgICAgICAgICAgICAgcmVzb2x2ZSh7cm9vbTogdGhpcy5yb29tLCByZWNvbm5lY3RlZDogcmVjb25uZWN0ZWR9KVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMub25jZShcInJvb20gam9pbmVkXCIsIGRhdGEgPT4geyBjcmVhdGVSb29tKGRhdGEsIGZhbHNlKSB9KVxyXG4gICAgICAgICAgICB0aGlzLm9uY2UoXCJyb29tIHJlY29ubmVjdFwiLCBkYXRhID0+IHsgY3JlYXRlUm9vbShkYXRhLCB0cnVlKSB9KVxyXG4gICAgICAgICAgICB0aGlzLm9uY2UoXCJyb29tIGpvaW4gZXJyb3JcIiwgZXJyb3IgPT4geyBjb25zb2xlLmVycm9yKFwiRXJyb3Igd2hpbGUgam9pbmluZyByb29tIFwiICsgaWQsIGVycm9yKSB9KVxyXG5cclxuICAgICAgICAgICAgdGhpcy5zZW5kKFwicm9vbSBqb2luXCIsIHtcclxuICAgICAgICAgICAgICAgIGlkOiBpZCxcclxuICAgICAgICAgICAgICAgIGRhdGE6IHt9XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfSlcclxuICAgIH1cclxuXHJcbiAgICBnZXRSb29tcygpOiBQcm9taXNlPFJvb21NZXRhW10+e1xyXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZTxSb29tTWV0YVtdPihyZXNvbHZlID0+IHtcclxuICAgICAgICAgICAgdGhpcy5vbmNlKFwicm9vbSBsaXN0XCIsIHJvb21zID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiUm9vb29vbXNcIiwgcm9vbXMpXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIHRoaXMuc2VuZChcInJvb20gbGlzdFwiKVxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcbiAgICBcclxuICAgIHNldHVwU29ja2V0KCl7XHJcbiAgICAgICAgdGhpcy5zb2NrZXQub25vcGVuID0gZXYgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNlbmQoXCJyZWdpc3RlclwiKVxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNvY2tldC5vbm1lc3NhZ2UgPSBldiA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IG1lc3NhZ2UgPSBKU09OLnBhcnNlKGV2LmRhdGEudG9TdHJpbmcoKSlcclxuICAgICAgICAgICAgdGhpcy5zZXNzaW9uX2lkID0gbWVzc2FnZS5zZXNzaW9uX2lkXHJcbiAgICAgICAgICAgIGxldCBkYXRhOiBhbnlcclxuICAgICAgICAgICAgdHJ5e1xyXG4gICAgICAgICAgICAgICAgZGF0YSA9IEpTT04ucGFyc2UobWVzc2FnZS5kYXRhKVxyXG4gICAgICAgICAgICB9Y2F0Y2goZSl7XHJcbiAgICAgICAgICAgICAgICBkYXRhID0gbWVzc2FnZS5kYXRhXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYobWVzc2FnZS5jb250ZXh0ID09PSBcIkFMTFwiKXtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2FsbChtZXNzYWdlLmV2ZW50LCBkYXRhKVxyXG4gICAgICAgICAgICB9ZWxzZSBpZihtZXNzYWdlLmNvbnRleHQgPT09IFwiUk9PTVwiKXtcclxuICAgICAgICAgICAgICAgIGlmKHRoaXMucm9vbSl7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJvb20uY2FsbChtZXNzYWdlLmV2ZW50LCBkYXRhKVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc29ja2V0Lm9uZXJyb3IgPSBlcnIgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycilcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zb2NrZXQub25jbG9zZSA9IChldikgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmRlYnVnKFwiQ29uZW5jdGlvbiBjbG9zZWRcIiwgZXYpXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLm9uY2UoXCJyZWNvbm5lY3RcIiwgKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5vbkNvbm5lY3RlZCh0cnVlKVxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgdGhpcy5vbmNlKFwicmVnaXN0ZXJlZFwiLCBzZXNzaW9uSWQgPT4ge1xyXG4gICAgICAgICAgICAvLyBrZWVwIHNlc3Npb24gaWQgZm9yIDMgZGF5c1xyXG4gICAgICAgICAgICBzZXNzaW9uU3RvcmFnZS5zZXRJdGVtKFwic2Vzc2lvbklkXCIsIHNlc3Npb25JZClcclxuICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2Uuc2V0SXRlbShcInNlc3Npb25JZEV4cGlyZVwiLCBuZXcgRGF0ZShuZXcgRGF0ZSgpLmdldFRpbWUoKSArIDEwMDAqNjAqNjAqMjQqMykudG9VVENTdHJpbmcoKSlcclxuICAgICAgICAgICAgdGhpcy5zZXNzaW9uX2lkID0gc2Vzc2lvbklkXHJcbiAgICAgICAgICAgIHRoaXMub25Db25uZWN0ZWQoZmFsc2UpXHJcbiAgICAgICAgfSlcclxuICAgICAgICB0aGlzLm9uKFwiZXJyb3JcIiwgZXJyb3IgPT4ge1xyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKVxyXG4gICAgICAgIH0pXHJcbiAgICB9XHJcblxyXG4gICAgXHJcbiAgICBzZW5kKGV2ZW50OiBzdHJpbmcsIGRhdGE6IGFueSA9IG51bGwsIGNvbnRleHQ6IHN0cmluZyA9IFwiQUxMXCIpe1xyXG4gICAgICAgIGxldCBtc2cgPSBkYXRhXHJcbiAgICAgICAgaWYoZGF0YSBpbnN0YW5jZW9mIE9iamVjdClcclxuICAgICAgICAgICAgbXNnID0gSlNPTi5zdHJpbmdpZnkoZGF0YSlcclxuICAgICAgICBjb25zb2xlLmRlYnVnKFwic2VuZGluZyBldmVudFwiLCBldmVudCwgZGF0YSlcclxuXHJcbiAgICAgICAgICAgIC8vIFRPRE86IENoZWNrIGlmIHNvY2tldCBpcyBjb25uZWN0ZWRcclxuICAgICAgICAgICAgdGhpcy5zb2NrZXQuc2VuZChKU09OLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgICAgIGV2ZW50OiBldmVudCxcclxuICAgICAgICAgICAgc2Vzc2lvbl9pZDogdGhpcy5zZXNzaW9uX2lkLFxyXG4gICAgICAgICAgICBjb250ZXh0OiBjb250ZXh0LFxyXG4gICAgICAgICAgICBkYXRhOiBtc2dcclxuICAgICAgICB9KSlcclxuICAgIH1cclxuICAgIFxyXG59IiwiaW1wb3J0IHsgRXZlbnRFbWl0dGVyIH0gZnJvbSBcIi4vZXZlbnQvRXZlbnRFbWl0dGVyXCI7XHJcbmltcG9ydCB7IEdhbWVDbGllbnQgfSBmcm9tIFwiLi9HYW1lQ2xpZW50XCJcclxuXHJcbmV4cG9ydCBjbGFzcyBSb29tIGV4dGVuZHMgRXZlbnRFbWl0dGVyIGltcGxlbWVudHMgUm9vbU1ldGF7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGlkLFxyXG4gICAgICAgIHB1YmxpYyBjb25maWd1cmF0aW9uLFxyXG4gICAgICAgIHB1YmxpYyBjbGllbnRcclxuICAgICl7XHJcbiAgICAgICAgc3VwZXIoKVxyXG4gICAgfVxyXG5cclxuXHJcbiAgICBzZW5kKGV2ZW50OiBzdHJpbmcsIGRhdGE6IGFueSl7IFxyXG4gICAgICAgIHRoaXMuY2xpZW50LnNlbmQoZXZlbnQsIGRhdGEsIFwiUk9PTTpcIiArIHRoaXMuaWQpXHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgY29uc3QgUm9vbUV2ZW50ID0ge1xyXG4gICAgREVTVFJPWTogXCJERVNUUk9ZXCIsXHJcbn0iLCJleHBvcnQgY2xhc3MgRXZlbnRFbWl0dGVye1xyXG4gICAgXHJcbiAgZXZlbnRIYW5kbGVycyA9IHt9XHJcblxyXG4gIG9uKGV2ZW50OiBzdHJpbmcsIGZ1bmM6IChkYXRhOiBhbnksIGV2ZW50OiBzdHJpbmcpID0+IHZvaWQpe1xyXG4gICAgaWYoIWV2ZW50IHx8ICFmdW5jKSByZXR1cm5cclxuICAgIGlmKCF0aGlzLmV2ZW50SGFuZGxlcnNbZXZlbnRdKXtcclxuICAgICAgdGhpcy5ldmVudEhhbmRsZXJzW2V2ZW50XSA9IFtdXHJcbiAgICB9XHJcbiAgICB0aGlzLmV2ZW50SGFuZGxlcnNbZXZlbnRdLnB1c2goZnVuYylcclxuICB9XHJcblxyXG4gIG9uY2UoZXZlbnQ6IHN0cmluZywgY2FsbGJhY2s6IChkYXRhOiBhbnkpID0+IHZvaWQpe1xyXG4gICAgdGhpcy5vbihcIiNvbmNlX1wiICsgZXZlbnQsIGNhbGxiYWNrKVxyXG4gIH1cclxuXHJcbiAgY2FsbChldmVudDogc3RyaW5nLCBkYXRhOiBhbnkpe1xyXG4gICAgaWYodGhpcy5ldmVudEhhbmRsZXJzW2V2ZW50XSl7XHJcbiAgICAgIHRoaXMuZXZlbnRIYW5kbGVyc1tldmVudF0uZm9yRWFjaChmdW5jID0+IGZ1bmMoZGF0YSkpXHJcbiAgICB9XHJcbiAgICBpZih0aGlzLmV2ZW50SGFuZGxlcnNbXCIqXCJdKXtcclxuICAgICAgdGhpcy5ldmVudEhhbmRsZXJzW1wiKlwiXS5mb3JFYWNoKGZ1bmMgPT4gZnVuYyhkYXRhLCBldmVudCkpXHJcbiAgICB9XHJcbiAgICBpZih0aGlzLmV2ZW50SGFuZGxlcnNbXCIjb25jZV9cIiArIGV2ZW50XSl7XHJcbiAgICAgIGNvbnN0IGZ1bmNzID0gdGhpcy5ldmVudEhhbmRsZXJzW1wiI29uY2VfXCIgKyBldmVudF1cclxuICAgICAgZm9yKGxldCBpID0gMDsgaSA8IGZ1bmNzLmxlbmd0aDsgaSsrKXtcclxuICAgICAgICBmdW5jc1tpXShkYXRhKVxyXG4gICAgICAgIHRoaXMuZXZlbnRIYW5kbGVyc1tcIiNvbmNlX1wiICsgZXZlbnRdLnNwbGljZShpLCAxKVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICByZW1vdmVFdmVudEhhbmRsZXIoZXZlbnQ6IHN0cmluZywgZnVuYyl7XHJcbiAgICBpZih0aGlzLmV2ZW50SGFuZGxlcnNbZXZlbnRdKXtcclxuICAgICAgY29uc3QgaW5kZXggPSB0aGlzLmV2ZW50SGFuZGxlcnNbZXZlbnRdLmluZGV4T2YoZiA9PiBmID09PSBmdW5jKVxyXG4gICAgICBpZihpbmRleCA+PSAwKXtcclxuICAgICAgICB0aGlzLmV2ZW50SGFuZGxlcnNbZXZlbnRdLnNwbGljZShpbmRleCwgMSlcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgcmVtb3ZlRXZlbnRIYW5kbGVycyhldmVudDogc3RyaW5nKXtcclxuICAgIHRoaXMuZXZlbnRIYW5kbGVyc1tldmVudF0gPSBbXVxyXG4gIH1cclxuXHJcbn0iLCJleHBvcnQgeyBHYW1lQ2xpZW50IH0gZnJvbSBcIi4vR2FtZUNsaWVudFwiXHJcbmV4cG9ydCB7IFJvb20sIFJvb21FdmVudCB9IGZyb20gXCIuL1Jvb21cIlxyXG5leHBvcnQgeyBFdmVudEVtaXR0ZXIgfSBmcm9tIFwiLi9ldmVudC9FdmVudEVtaXR0ZXJcIiJdLCJzb3VyY2VSb290IjoiIn0=